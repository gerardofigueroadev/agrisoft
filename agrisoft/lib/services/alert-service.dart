import 'package:flutter/material.dart';
import 'package:agrisoft/helper/AlertModel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

Future<List<AlertModel>> getAll() async {
  List<AlertModel> alerts = [];
  AlertModel aux;
  QuerySnapshot snap =
      await Firestore.instance.collection('alerts').getDocuments();
  snap.documents.forEach((document) => {
        print(document.toString()),
        aux = AlertModel.fromJson(document.data, document.documentID),
        alerts.add(aux),
      });
  print(alerts[0].createdBy);
  return alerts;
}

Widget _buildBody(BuildContext context) {
  return StreamBuilder<QuerySnapshot>(
    stream: Firestore.instance.collection('alerts').snapshots(),
    builder: (context, snapshot) {
      if (!snapshot.hasData) return LinearProgressIndicator();
      print(snapshot);
      //return _buildList(context, snapshot.data.documents);
    },
  );
}
