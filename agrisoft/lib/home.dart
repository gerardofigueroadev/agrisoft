import 'package:agrisoft/alerts.dart';
import 'package:agrisoft/learnings.dart';
import 'package:agrisoft/profile.dart';
import 'package:flutter/material.dart';
import 'package:motion_tab_bar/MotionTabBarView.dart';
import 'package:motion_tab_bar/MotionTabController.dart';
import 'package:motion_tab_bar/motiontabbar.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  MotionTabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = MotionTabController(initialIndex: 1, vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: MotionTabBar(
          labels: ["Perfil", "Alerta", "Aprende"],
          initialSelectedTab: "Alerta",
          tabIconColor: Colors.black54,
          tabSelectedColor: Colors.green,
          onTabItemSelected: (int value) {
            setState(() {
              _tabController.index = value;
            });
          },
          icons: [Icons.account_circle, Icons.add_alert, Icons.library_books],
          textStyle: TextStyle(color: Colors.green),
        ),
        body: MotionTabBarView(
          controller: _tabController,
          children: <Widget>[
            Container(child: new ProfilePage()),
            Container(child: new AlertsPage()),
            Container(child: new LearningsPage()),
          ],
        ));
  }
}