class AlertModel {
  String id;
  String name;
  String description;
  String createdBy;
  String category;

  AlertModel(
      {this.id, this.name, this.description, this.createdBy, this.category});

  factory AlertModel.fromJson(Map<String, dynamic> json, String id) {
    return AlertModel(
        id: id,
        name: "name",
        description: json["description"],
        createdBy: json["createdBy"],
        category: json["category"]);
  }
}

class AlertList {
  static List<AlertModel> list = [
    AlertModel(
        name: "Alerta 1",
        description:
            "Launch your career in data science. A sweet-cource introduction to data science, develop and taught by leading professors.",
        createdBy: "Jons Hopkins University",
        category: "Data science"),
    AlertModel(
        name: "Alerta 2",
        description:
            "This specialization from leading researchers at university of washington introduce to you to the exciting high-demand field of machine learning ",
        createdBy: "University of washington",
        category: "Machine Learning"),
    AlertModel(
        name: "Alerta 3",
        description:
            "Drive better bussiness decision with an overview OF how big data is organised  and intepreted. Apply insight to real-world problems and question",
        createdBy: "Us San Diego",
        category: "Data Dataa"),
    AlertModel(
        name: "Alerta 4",
        description:
            "Drive better bussiness decision with an overview OF how big data is organised  and intepreted. Apply insight to real-world problems and question",
        createdBy: "Us San Diego",
        category: "Data Data")
  ];
}
